import subprocess
import json
from flask import Flask, render_template, jsonify

app = Flask(__name__)

OCCI_COMMON = "/usr/local/bin/occi --auth x509 --user-cred /tmp/x509up_u1000 --voms --o json "
OCCI_ENDPOINT = "--endpoint https://carach5.ics.muni.cz:11443 "
A_DESCRIBE = "--action describe "
A_LIST = "--action list "
A_DELETE = "--action delete "

FLAVORS_FILENAME = "flavors.json"
IMAGES_FILENAME = "images.json"


def get_list_of_resource(type_of_resource):
    """Return list of available resources"""
    r_type = "--resource " + type_of_resource
    command = OCCI_COMMON + OCCI_ENDPOINT + A_LIST + r_type
    de_result = subprocess.check_output(command, shell=True)
    occi_json = json.loads(de_result)
    return occi_json

def describe_resource_by_id(resource_id):
    """Describe resource by id"""
    r_string = "--resource " + resource_id
    command = OCCI_COMMON + OCCI_ENDPOINT + A_DESCRIBE + r_string
    de_result = subprocess.check_output(command, shell=True)
    resource = json.loads(de_result)
    return resource

def delete_resource_by_id(resource_id):
    """Delete resource"""
    r_string = "--resource " + resource_id
    command = OCCI_COMMON + OCCI_ENDPOINT + A_DELETE + r_string
    de_result = subprocess.check_output(command, shell=True)

def list_resources_w_d(resource_type):
    """ Vraci list zdroju s detaily """
    described = []
    get_os_list_path = OCCI_COMMON + OCCI_ENDPOINT + A_LIST + "--resource " + resource_type
    output = subprocess.check_output(get_os_list_path, shell=True)
    resources_json = json.loads(output)
    for item in resources_json:
        described.append(describe_resource_by_id(item))
    return described





@app.before_first_request
def get_site_info():
    """Load images and flavors info and store to files."""
    flavors = list_resources_w_d("resource_tpl")
    images = list_resources_w_d("os_tpl")

    with open(FLAVORS_FILENAME, 'w') as flavor_f:
        json.dump(flavors, flavor_f)

    with open(IMAGES_FILENAME, 'w') as images_f:
        json.dump(images, images_f)


@app.route('/')
def list_of_resources():
    """Index"""
    computes = get_list_of_resource("compute")
    storages = get_list_of_resource("storage")
    endpoint = OCCI_ENDPOINT.split()[1]
    return render_template('list.html', computes=computes, storages=storages, endpoint=endpoint)


@app.route('/describe_resource/<path:resource_id>')
def describe_resource(resource_id):
    """Show resource as json"""
    resource = describe_resource_by_id(resource_id)
    return jsonify(resource)

@app.route('/delete_resource/<path:resource_id>')
def delete_resource(resource_id):
    """Delete occi resource"""
    delete_resource_by_id(resource_id)
    return render_template('delete.html', resource=resource_id)

@app.route('/images')
def list_of_images():
    """Show images and flavors"""
    with open(IMAGES_FILENAME, 'r') as f_obj:
        images = json.load(f_obj)
    with open(FLAVORS_FILENAME, 'r') as f_obj:
        flavors = json.load(f_obj)
    return render_template('im_fl.html', flavors=flavors, images=images)
#    return jsonify(flavors)


@app.route('/show_images_json')
def show_images_json():
    """Show images as json"""
    with open(IMAGES_FILENAME, 'r') as images_f:
        im = json.load(images_f)
    return jsonify(im)

@app.route('/show_flavors_json')
def show_flavors_json():
    """Show flavors as json"""
    with open(FLAVORS_FILENAME, 'r') as flavor_f:
        fl = json.load(flavor_f)
    return jsonify(fl)


if __name__ == '__main__':
    app.run(debug=True)
